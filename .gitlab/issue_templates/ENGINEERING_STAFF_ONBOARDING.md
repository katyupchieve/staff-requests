- [ ] Create main accounts - Admin
  - [ ] Googlex
  - [ ] Zoom
    - [ ] Install Zoom calendar browser extension and sign in - Employee
  - [ ] Calendly
  - [ ] Monday
  - [ ] Create Microsoft account
    - [ ] Add account to Azure Portal access group
  - [ ] Create a 1Password account - Admin
    - [ ] Download 1password desktop and phone apps
    - [ ] 1Password tutorial - Admin & Employee
  - [ ] Staff member makes a Gitlab account - Employee
    - [ ] Account is added to UPchieve org - Admin
  - [ ] Sentry
  - [ ] Posthog
  - [ ] Zendesk
  - [ ] Doppler
  - [ ] SendGrid
  - [ ] Twilio
  - [ ] Aiven
  - [ ] Allma - Employee logs in with Slack
  - [ ] NewRelic
  - [ ] PagerDuty
  - [ ] FreshStatus 
- [ ] Invite employee to weekly OSS meeting - Admin 
- [ ]  Add to Slacks - Admin
  - [ ]  Staff - Admin
  - [ ]  Community - Admin
- [ ] Clone main repositories - Employee 
  - [ ] Clone and run appropriate [tower-crane](https://gitlab.com/upchieve/tower-crane) script - Employee
  - [ ] Clone [subway](https://gitlab.com/upchieve/subway)
      - [ ] Run `$ npm i && npm run test` and make sure tests pass
  - [ ] Clone [port-authority](https://gitlab.com/upchieve/port-authority)
  - [ ] Clone [grand-central-station](https://gitlab.com/upchieve/grand-central-station)
- [ ] Onboard to development tools
  - [ ] Walkthrough of Gitlab
  - [ ] Walkthrough of Doppler
  - [ ] Employee makes a deployable change
  - [ ] Tutorial on ArgoCD and Ambassador Rollouts
  - [ ] Tutorial on Allma
- [ ] Book time with other staff for meet n' greet - Employee
- [ ] Welcome activity is scheduled - Admin
